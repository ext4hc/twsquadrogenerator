﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace AltiumDesignerParser.Model
{
    class Component
    {
        string altiumComponent;
        public string AltiumComponent
        {
            get => altiumComponent;
            set
            {
                altiumComponent = value;
            }
        }

        string machineComponent;
        public string MachineComponent
        {
            get => machineComponent;
            set
            {
                machineComponent = value;
            }
        }
    }
}
