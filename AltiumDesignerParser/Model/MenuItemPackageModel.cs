﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltiumDesignerParser.Model
{
    class MenuItemPackageModel
    {
        public string Name { get; set; }

        public MenuItemPackageModel(string name)
        {
            Name = name;
        }
    }
}
