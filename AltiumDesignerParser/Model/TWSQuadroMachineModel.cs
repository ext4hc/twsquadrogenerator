﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AltiumDesignerParser.Services;

namespace AltiumDesignerParser
{
    class TWSQuadroMachineModel : Notify
    {
        const int stepDegree = 90;
        const int maxDegree = 360;

        public string Designator { get; set; }
        public string FootPrint { get; set; }
        public string Package { get; set; }
        public float CoordinateX { get; set; }
        public float CoordinateY { get; set; }
        public float Rotate { get; set; }
        public string Layer { get; set; }
        public string Comments { get; set; }
        public string TipoComponent { get; set; }



        public bool IsAssemblyFile { get; set; } = true;
        public string Feeder { get; set; }
        public string Orient { get; set; }
        public string Nozzle { get; set; }

        TWSQuadroMachineModel(AltiumParam AParam)
        {
            Designator = AParam.Designator;
            FootPrint = AParam.FootPrint;
            CoordinateX = AParam.CenterX;
            CoordinateY = AParam.CenterY;
            Rotate = AParam.Rotation;
            Layer = AParam.Layer;
            Comments = AParam.Comments;

            Package = null;
            Feeder = null;
            Orient = null;
            Nozzle = null;
            TipoComponent = null;
        }

        public static TWSQuadroMachineModel Create(AltiumParam AParam)
        {
            return new TWSQuadroMachineModel(AParam);
        }


        public float RotateChangeUp
        {
            get => Rotate + stepDegree > maxDegree ? 0 : Rotate + stepDegree;
        }
        public float RotateChangeDown
        {
            get => Rotate - stepDegree < 0 ? maxDegree : Rotate - stepDegree;
        }
    }
}
