﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AltiumDesignerParser.Services;

namespace AltiumDesignerParser.Model
{
    class GlobalStateFilterComponent
    {
        public object Parameter { get; set; }
        public string Name { get; set; }
        public bool? State { get; set; }

        public GlobalStateFilterComponent(object parameter, string propertyName, bool state)
        {
            Parameter = parameter;
            Name = propertyName;
            State = state;
        }
    }

    class FilterModel
    {
        MenuItemFileViewModel _qMachines;
        GlobalStateFilterComponent[] _statesFilter;

        public bool IsAssemblyDisplay { get; set; } = true;
        public bool IsPackageDisplay { get; set; }
        public bool IsFeederDisplay { get; set; }
        public bool IsBottomDisplay { get; set; }
        public bool IsTopDisplay { get; set; }

        public int QntTotalElements { get; set; }
        public int QntVisableElements { get; set; }
        public int QntRestElements { get; set; }

        public bool IsModePackage { get; set; } = true;
        public bool IsModeFeeder { get; set; }
        public bool IsModeNozzle { get; set; }


        public FilterModel(MenuItemFileViewModel qMachines)
        {
            _qMachines = qMachines;
        }

        public void SetContextFilter(GlobalStateFilterComponent[] statesFilter)
        {
            _statesFilter = statesFilter;
        }

        public int GetRestElementsAfterModif(string propertyName)
        {
            foreach(var state in _statesFilter)
            {
                if (state.State == true && propertyName == state.Name)
                    return QntRestElements - 1;
            }
            return QntRestElements;
        }

        public bool GetRestElementsAfterModifBool(string propertyName)
        {
            foreach (var state in _statesFilter)
            {
                if (state.State == true && propertyName == state.Name)
                    return true;
            }
            return false;
        }

        public bool CheckElementColorFilter()
        {
            bool allPredicate = true;
            foreach(var filter in _statesFilter)
            {
                if (filter.State == true)
                    allPredicate = false;
            }

            if (allPredicate)
                return false;

            foreach(var filter in _statesFilter)
            {
                if (filter.State == true && filter.Parameter == null)
                    return false;
            }

            return true;
        }
    }
}
