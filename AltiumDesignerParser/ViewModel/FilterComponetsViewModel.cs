﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using AltiumDesignerParser.Model;
using AltiumDesignerParser.Services;

namespace AltiumDesignerParser.ViewModel
{
    class FilterComponetsViewModel : Notify
    {
        List<TWSQuadroMachineViewModel> QntFilteredElements = new List<TWSQuadroMachineViewModel>();
        MenuItemFileViewModel _machinesList;
        FilterModel _filterModel;
        ObservableCollection<TWSQuadroMachineViewModel> filteredMachinesList = new ObservableCollection<TWSQuadroMachineViewModel>();
        public ObservableCollection<TWSQuadroMachineViewModel> FilteredMachinesList
        {
            get => filteredMachinesList;
            set
            {
                filteredMachinesList = value;
                NotifyPropertyChanged();
            }
        }

        public FilterComponetsViewModel(MenuItemFileViewModel machinesList)
        {
            _machinesList = machinesList;
            _machinesList.OpenedFile += _machinesList_OpenedFile;
            _filterModel = new FilterModel(_machinesList);
        }

        private void _machinesList_OpenedFile()
        {
            FilteredMachinesList.Clear();
            QntFilteredElements.Clear();
            foreach (var machine in _machinesList.TWSQuadroMachinesList)
            {
                machine.PropertyChanged += Machine_PropertyChanged;
                FilteredMachinesList.Add(machine);
                //QntFilteredElements.Add(machine);
            }

            QntTotalElements = _machinesList.TWSQuadroMachinesList.Count;
            QntVisableComponents = FilteredMachinesList.Count;
            SetFilterColor.Execute(null);
        }

        private void Machine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            TWSQuadroMachineViewModel MachineViewModel = sender as TWSQuadroMachineViewModel;
            if (e.PropertyName == "IsAssemblyFile")
            {
                FilteredMachinesList.Remove(MachineViewModel);
                QntVisableComponents = FilteredMachinesList.Count;
                //QntRestElements = MachineViewModel.UnderColor != ColorService.TrueComponentColor ? QntRestElements - 1 : QntRestElements;

                if(MachineViewModel.UnderColor != ColorService.TrueComponentColor)
                {
                    QntFilteredElements.Remove(MachineViewModel);
                    QntRestElements = QntFilteredElements.Count;
                }

            }

            if (e.PropertyName == "Package" || e.PropertyName == "Feeder" || e.PropertyName == "Nozzle")
            {

                _filterModel.SetContextFilter(new GlobalStateFilterComponent[]
                {
                    new GlobalStateFilterComponent(MachineViewModel.Package, "Package", IsModePackage),
                    new GlobalStateFilterComponent(MachineViewModel.Feeder, "Feeder", IsModeFeeder),
                    new GlobalStateFilterComponent(MachineViewModel.Nozzle, "Nozzle", IsModeNozzle)
                });

                if(QntFilteredElements.Contains(MachineViewModel))
                {
                    if (_filterModel.CheckElementColorFilter())
                    {
                        MachineViewModel.UnderColor = ColorService.TrueComponentColor;
                        QntFilteredElements.Remove(MachineViewModel);
                    }
                }

                QntRestElements = QntFilteredElements.Count;
            }

        }

        public bool IsModePackage
        {
            get => _filterModel.IsModePackage;
            set
            {
                _filterModel.IsModePackage = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsModeFeeder
        {
            get => _filterModel.IsModeFeeder;
            set
            {
                _filterModel.IsModeFeeder = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsModeNozzle
        {
            get => _filterModel.IsModeNozzle;
            set
            {
                _filterModel.IsModeNozzle = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsAssemblyDisplay
        {
            get => _filterModel.IsAssemblyDisplay;
            set
            {
                _filterModel.IsAssemblyDisplay = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsPackageDisplay
        {
            get => _filterModel.IsPackageDisplay;
            set
            {
                _filterModel.IsPackageDisplay = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsFeederDisplay
        {
            get => _filterModel.IsFeederDisplay;
            set
            {
                _filterModel.IsFeederDisplay = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsTopDisplay
        {
            get => _filterModel.IsTopDisplay;
            set
            {
                _filterModel.IsTopDisplay = value;
                if(_filterModel.IsTopDisplay == true)
                {
                    _filterModel.IsBottomDisplay = !_filterModel.IsTopDisplay;
                    NotifyPropertyChanged("IsBottomDisplay");
                }
                NotifyPropertyChanged();
            }
        }

        public bool IsBottomDisplay
        {
            get => _filterModel.IsBottomDisplay;
            set
            {
                _filterModel.IsBottomDisplay = value;
                if(_filterModel.IsBottomDisplay == true)
                {
                    _filterModel.IsTopDisplay = !_filterModel.IsBottomDisplay;
                    NotifyPropertyChanged("IsTopDisplay");
                }
                NotifyPropertyChanged();
            }
        }

        public int QntTotalElements
        {
            get => _filterModel.QntTotalElements;
            set
            {
                _filterModel.QntTotalElements = value;
                NotifyPropertyChanged();
            }
        }

        public int QntVisableComponents
        {
            get => _filterModel.QntVisableElements;
            set
            {
                _filterModel.QntVisableElements = value;
                NotifyPropertyChanged();
            }
        }

        public int QntRestElements
        {
            get => _filterModel.QntRestElements;
            set
            {
                _filterModel.QntRestElements = value;
                NotifyPropertyChanged();
            }
        }

        CustomCommand setFilterColor;
        public CustomCommand SetFilterColor
        {
            get
            {
                return setFilterColor ?? (setFilterColor = new CustomCommand(param =>
                {

                    QntFilteredElements.Clear();
                    foreach(var el in FilteredMachinesList)
                    {
                        _filterModel.SetContextFilter(new GlobalStateFilterComponent[]
                        {
                            new GlobalStateFilterComponent(el.Package, "Package", IsModePackage),
                            new GlobalStateFilterComponent(el.Feeder, "Feeder", IsModeFeeder),
                            new GlobalStateFilterComponent(el.Nozzle, "Nozzle", IsModeNozzle)
                        });

                        if(_filterModel.CheckElementColorFilter())
                        {
                            el.UnderColor = ColorService.TrueComponentColor;
                        }
                        else
                        {
                            el.UnderColor = ColorService.FalseComponentColor;
                            QntFilteredElements.Add(el);
                        }

                    }
                    QntRestElements = QntFilteredElements.Count;

                }, param => _machinesList.TWSQuadroMachinesList.Count() != 0));
            }
        }

        CustomCommand displayComponents;
        public CustomCommand DisplayComponents
        {
            get
            {
                return displayComponents ?? (displayComponents = new CustomCommand(param =>
                {
                    IsModeFeeder = false;
                    IsModeNozzle = false;
                    IsModePackage = false;

                    foreach (var component in _machinesList.TWSQuadroMachinesList)
                    {
                        component.UnderColor = ColorService.FalseComponentColor;
                    }

                    FilteredMachinesList.Clear();
                    QntFilteredElements.Clear();

                    var filter = _machinesList.TWSQuadroMachinesList.Where(o => o.IsAssemblyFile == IsAssemblyDisplay);

                    if (IsTopDisplay)
                        filter = filter.Where(o => o.Layer == "TopLayer");

                    if (IsBottomDisplay)
                        filter = filter.Where(o => o.Layer == "BottomLayer");


                    if (IsFeederDisplay)
                        filter = filter.Where(o => o.Feeder != null);

                    if (IsPackageDisplay)
                        filter = filter.Where(o => o.Package != null);

                    foreach (TWSQuadroMachineViewModel machine in filter)
                    {
                        FilteredMachinesList.Add(machine);
                        QntFilteredElements.Add(machine);
                    }

                    QntVisableComponents = filter.Count();
                    QntRestElements = QntFilteredElements.Count;
                }, param => _machinesList.TWSQuadroMachinesList.Count() != 0));
            }
        }
    }
}
