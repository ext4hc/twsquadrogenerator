﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows;
using System.IO;
using Microsoft.Win32;
using AltiumDesignerParser.Services;

namespace AltiumDesignerParser.ViewModel
{
    class FeederViewModel : Notify
    {
        MenuItemFileViewModel _machineList;
        FeederParse FeederParser;
        List<Feeder> FeederParams;
        List<Feeder> LocalFeederParams = new List<Feeder>();

        static private string pathFeederFile = "feeder.csv";

        public List<Feeder> Machines
        {
            get
            {
                return FeederParams;
            }
            set
            {
                FeederParams = value;
                NotifyPropertyChanged();
            }
        }

        public FeederViewModel(MenuItemFileViewModel machinesList)
        {
            _machineList = machinesList;

            InitFeeders();
            //LocalFeederParams.AddRange(FeederParams.ToArray());

            if (_machineList.TWSQuadroMachineSelected != null)
            {
                Content = _machineList.TWSQuadroMachineSelected.Feeder;
            }


        }

        private void InitFeeders()
        {
            FeederParser = new FeederParse(pathFeederFile);
            FeederParams = new List<Feeder>();
            foreach (var el in FeederParser.Parse().Where(p => p.Package == _machineList.TWSQuadroMachineSelected?.Package))
            {
                FeederParams.Add(el);
            }
            NotifyPropertyChanged("Machines");
        }

        string content;
        public string Content
        {
            get => content;
            set
            {
                content = value;
                NotifyPropertyChanged();
            }
        }

        public string PathFeederFile
        {
            get => Path.GetFullPath(pathFeederFile);
            set
            {
                pathFeederFile = value;
                NotifyPropertyChanged();
            }
        }

        bool isChangeAllComponent = true;
        public bool IsChangeAllComponent
        {
            get => isChangeAllComponent;
            set
            {
                isChangeAllComponent = value;
                NotifyPropertyChanged();
            }
        }

        CustomCommand openWindow;
        public CustomCommand OpenWindow
        {
            get
            {
                return openWindow ?? (openWindow = new CustomCommand(param =>
                {
                    WindowGenerate.CustomWinCreate<SetFeeder>(new FeederViewModel(_machineList));
                }, param => _machineList.TWSQuadroMachineSelected != null));
            }
        }

        CustomCommand openFeederFile;
        public CustomCommand OpenFeederFile => openFeederFile ?? (openFeederFile = new CustomCommand(param =>
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                InitialDirectory = "D:\\",
                Filter = "Feeder файл|*.csv"
            };

            if(dialog.ShowDialog() == true)
            {
                PathFeederFile = dialog.FileName;
                InitFeeders();

                //FeederParser = new FeederParse(pathFeederFile);
                //FeederParams = FeederParser.Parse();

                //LocalFeederParams.Clear();
                //LocalFeederParams.AddRange(FeederParams.ToArray());
                //Machines = FeederParams;
                
                
            }
        }, param => true));

        CustomCommand save;
        public CustomCommand Save
        {
            get
            {
                return save ?? (save = new CustomCommand(param =>
                {

                    if (param is Window window)
                    {
                        if (Int32.TryParse(Content, out int result))
                        {
                            var tipoComp = Machines.Where(p => p.FeederVal == Content).Select(p => p.Component).ToArray()[0];

                            if (IsChangeAllComponent)
                            {
                                var filtered = _machineList.TWSQuadroMachinesList.Where(p => p.FootPrint == _machineList.TWSQuadroMachineSelected.FootPrint).Where(
                                                                                        p => p.Comments == _machineList.TWSQuadroMachineSelected.Comments);

                                foreach (var el in filtered)
                                {
                                    el.Feeder = Content;
                                    el.TipoComponent = tipoComp;
                                }

                            }
                            else
                            {
                                _machineList.TWSQuadroMachineSelected.Feeder = Content;
                                _machineList.TWSQuadroMachineSelected.TipoComponent = tipoComp;
                            }

                        }
                        window.Close();
                    }

                }, param => true));
            }
        }

        CustomCommand cancel;
        public CustomCommand Cancel => cancel ?? (cancel = new CustomCommand(param =>
        {
            if (param is Window window)
            {
                window.Close();
            }
        }, param => true));
    }
}
