﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AltiumDesignerParser.ViewModel;
using AltiumDesignerParser.View;
using AltiumDesignerParser.Services;


namespace AltiumDesignerParser.ViewModel
{
    class MenuItemEdit
    {
        MenuItemFileViewModel _fileMV;

        public MenuItemEdit(MenuItemFileViewModel fileMV)
        {
            _fileMV = fileMV;
        }

        CustomCommand setOriginDot;
        public CustomCommand SetOriginDot => setOriginDot ?? (setOriginDot = new CustomCommand(action =>
        {
            WindowGenerate.CustomWinCreate<SetZero>(new ZeroBoardViewModel(_fileMV));
        }, exec => _fileMV.TWSQuadroMachinesList.Count != 0));
    }
}
