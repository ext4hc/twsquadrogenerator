﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using AltiumDesignerParser.Services;
using AltiumDesignerParser.Model;
using AltiumDesignerParser.ViewModel;

namespace AltiumDesignerParser.ViewModel
{

    


    class SubItemMenuPackagesViewModel
    {
        public ObservableCollection<MenuItemPackageViewModel> PackagesMenuItem { get; }
        MenuItemFileViewModel _machinesList;
        ToolBarComponentEditViewModel _toolBarComponentEdit;

        public SubItemMenuPackagesViewModel(MenuItemFileViewModel machinesList, ToolBarComponentEditViewModel toolBarComponentEdit)
        {
            PackagesMenuItem = new ObservableCollection<MenuItemPackageViewModel>();
            _machinesList = machinesList;
            _toolBarComponentEdit = toolBarComponentEdit;

            foreach (string line in MachineComponents.GetInstance().Components)
            {
                var packageModel = new MenuItemPackageModel(line);

                PackagesMenuItem.Add(new MenuItemPackageViewModel(packageModel, SelectedMenuItem));
            }
        }

        CustomCommand selectedMenuItem;
        public CustomCommand SelectedMenuItem
        {
            get
            {
                return selectedMenuItem ?? (selectedMenuItem = new CustomCommand(obj =>
                {

                    if (obj is string namePackage)
                    {
                        _toolBarComponentEdit.SelectedAvailablePackage = namePackage;
                    }
                }, obj => _machinesList.TWSQuadroMachineSelected != null));
            }
        }
    }
}
