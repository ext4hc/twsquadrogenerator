﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AltiumDesignerParser.Services;
using System.Data.SQLite;
using System.IO;
using System.Threading;
using AltiumDesignerParser.Model;
using Microsoft.Win32;

namespace AltiumDesignerParser.ViewModel
{
    class GeneralViewModel : Notify
    {
        public MenuItemFileViewModel MenuItemFile { get; set; }
        public ToolBarComponentEditViewModel ToolbarEdit { get; set; }
        public SubItemMenuPackagesViewModel Packages { get; set; }
        public FeederViewModel Feeder { get; set; }
        public FilterComponetsViewModel FilterComponents { get; set; }
        public MenuItemOutputViewModel OutputFile { get; set; }
        public MenuItemEdit Edit { get; set; }

        public GeneralViewModel()
        {

            
            SQLiteServices.CreateDataBase("Comp.db");
            SQLiteServices.CreateTable("Components");

            MenuItemFile = new MenuItemFileViewModel();
            ToolbarEdit = new ToolBarComponentEditViewModel(MenuItemFile);
            Packages = new SubItemMenuPackagesViewModel(MenuItemFile, ToolbarEdit);
            Feeder = new FeederViewModel(MenuItemFile);
            FilterComponents = new FilterComponetsViewModel(MenuItemFile);
            OutputFile = new MenuItemOutputViewModel(MenuItemFile);
            Edit = new MenuItemEdit(MenuItemFile);
        }


    }

}
