﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AltiumDesignerParser.Services;
using AltiumDesignerParser.Model;
using System.Windows;

namespace AltiumDesignerParser
{
    class MenuItemFileViewModel : Notify
    {
        public ObservableCollection<TWSQuadroMachineViewModel> TWSQuadroMachinesList { get; set; }
        public ObservableCollection<string> AVComponents { get; set; }

        AltiumParse Altium;
        public delegate void NotifyViewModel();
        public event NotifyViewModel SelectedMachineItem;
        public event NotifyViewModel OpenedFile;

        TWSQuadroMachineViewModel twsQuadroMachineSelected;
        public TWSQuadroMachineViewModel TWSQuadroMachineSelected
        {
            get => twsQuadroMachineSelected;
            set
            {
                twsQuadroMachineSelected = value;
                SelectedMachineItem();
                NotifyPropertyChanged();
            }
        }

        public MenuItemFileViewModel()
        {
            TWSQuadroMachinesList = new ObservableCollection<TWSQuadroMachineViewModel>();
            AVComponents = new ObservableCollection<string>();

            foreach (string package in MachineComponents.GetInstance().Components)
            {
                AVComponents.Add(package);
            }
        }


        CustomCommand openFile;
        public CustomCommand OpenFile
        {
            get
            {
                return openFile ?? (openFile = new CustomCommand(o =>
                {
                    OpenFileDialog dialog = new OpenFileDialog()
                    {
                        InitialDirectory = "D:\\",
                        Filter = "Altium файл|*.csv"
                    };

                    if (dialog.ShowDialog() == true)
                    {
                        List<Component> Components = SQLiteServices.GetAllDataFromDataBase();
                        //Altium = new AltiumParse("Pick Place for ID-F Indication.csv");
                        Altium = new AltiumParse(dialog.FileName);
                        TWSQuadroMachinesList.Clear();

                        foreach (AltiumParam Param in Altium.Parse())
                        {
                            TWSQuadroMachinesList.Add(new TWSQuadroMachineViewModel(TWSQuadroMachineModel.Create(Param)));

                            Component component = Components.Find(param => param.AltiumComponent == TWSQuadroMachinesList.Last().FootPrint);
                            if (component != null)
                                TWSQuadroMachinesList.Last().Package = component.MachineComponent;
                        }
                        OpenedFile();
                    }

                }, (o) => true));
            }
        }

        CustomCommand closeProgram;
        public CustomCommand CloseProgram => closeProgram ?? (closeProgram = new CustomCommand(param =>
        {
            if(param is Window window)
            {
                window.Close();
            }
        }, param => true));
    }
}
