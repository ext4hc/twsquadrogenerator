﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AltiumDesignerParser.Services;
using AltiumDesignerParser.Model;
using AltiumDesignerParser.ViewModel;

namespace AltiumDesignerParser
{
    class TWSQuadroMachineViewModel : Notify
    {
        TWSQuadroMachineModel _component;

        public TWSQuadroMachineViewModel(TWSQuadroMachineModel component)
        {
            _component = component;
        }
            

        string underColor = ColorService.FalseComponentColor;
        public string UnderColor
        {
            get => underColor;
            set
            {
                underColor = value;
                NotifyPropertyChanged();
            }
        }

        public string Designator
        {
            get => _component.Designator;
            set
            {
                _component.Designator = value;
                NotifyPropertyChanged();
            }
        }

        public string Comments
        {
            get => _component.Comments;
            set
            {
                _component.Comments = value;
                NotifyPropertyChanged();
            }
        }

        public string FootPrint
        {
            get => _component.FootPrint;
            set
            {
                _component.FootPrint = value;
                NotifyPropertyChanged();
            }
        }

        public string Package
        {
            get => _component.Package;
            set
            {
                _component.Package = value;
                NotifyPropertyChanged();
            }
        }

        public float CoordinateX
        {
            get => _component.CoordinateX;
            set
            {
                _component.CoordinateX = value;
                NotifyPropertyChanged();
            }
        }

        public float CoordinateY
        {
            get => _component.CoordinateY;
            set
            {
                _component.CoordinateY = value;
                NotifyPropertyChanged();
            }
        }

        public float Rotate
        {
            get => _component.Rotate;
            set
            {
                _component.Rotate = value;
                NotifyPropertyChanged();
            }
        }

        public string TipoComponent
        {
            get => _component.TipoComponent;
            set
            {
                _component.TipoComponent = value;
                NotifyPropertyChanged();
            }
        }

        public string Layer
        {
            get => _component.Layer;
            set
            {
                _component.Layer = value;
                NotifyPropertyChanged();
            }
        }

        public string Feeder
        {
            get => _component.Feeder;
            set
            {
                _component.Feeder = value;
                NotifyPropertyChanged();
            }
        }

        public string Nozzle
        {
            get => _component.Nozzle;
            set
            {
                _component.Nozzle = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsAssemblyFile
        {
            get => _component.IsAssemblyFile;
            set
            {
                _component.IsAssemblyFile = value;
                NotifyPropertyChanged();
            }
        }

        public void RotateChangeUp()
        {
            Rotate = _component.RotateChangeUp;
        }

        public void RotateChangeDown()
        {
            Rotate = _component.RotateChangeDown;
        }
    }
}
