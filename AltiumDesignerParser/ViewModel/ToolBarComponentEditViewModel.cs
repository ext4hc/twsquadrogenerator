﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using AltiumDesignerParser.Services;
using AltiumDesignerParser.ViewModel;

namespace AltiumDesignerParser
{

    class ToolBarComponentEditViewModel : Notify
    {
        MenuItemFileViewModel _machinesList;
        List<string> LocalAVComponents = new List<string>();
        public ToolBarComponentEditViewModel(MenuItemFileViewModel MachinesList)
        {
            _machinesList = MachinesList;
            _machinesList.SelectedMachineItem += UpdatePackageView;
            LocalAVComponents.AddRange(_machinesList.AVComponents.ToArray());
        }


        public void UpdatePackageView()
        {
            if (_machinesList.TWSQuadroMachineSelected.Package != null)
            {
                SelectedAvailablePackage = _machinesList.TWSQuadroMachineSelected.Package;
            }
            else
            {
                SelectedAvailablePackage = null;
            }
        }
        

        string selectedAvailablePackage;
        public string SelectedAvailablePackage
        {
            get
            {
                return selectedAvailablePackage ?? "";
            }
            set
            {
                if (selectedAvailablePackage == value || value == null || value == _machinesList.TWSQuadroMachineSelected.Package)
                    return;

                selectedAvailablePackage = value;
                var element = _machinesList.TWSQuadroMachinesList.Where(footrprint => String.Equals(footrprint.FootPrint,
                                                          _machinesList.TWSQuadroMachineSelected.FootPrint));

                foreach (TWSQuadroMachineViewModel package in element)
                {
                    package.Package = selectedAvailablePackage;
                }

                SQLiteServices.InsertEntry(new Model.Component() { AltiumComponent = _machinesList.TWSQuadroMachineSelected.FootPrint,
                                                                   MachineComponent = _machinesList.TWSQuadroMachineSelected.Package});

                NotifyPropertyChanged();
            }
        }

        string filterSearchPackage;
        public string FilterSearchPackage
        {
            get => filterSearchPackage;
            set
            {
                var temp = LocalAVComponents.Where(obj => obj.ToLower().StartsWith(value.ToLower()));
                _machinesList.AVComponents.Clear();

                foreach (string machine in temp)
                {
                    _machinesList.AVComponents.Add(machine);
                }

                filterSearchPackage = value;
                NotifyPropertyChanged();
            }
        }

        CustomCommand changeRotateCommandUp;
        public CustomCommand ChangeRotateCommandUp
        {
            get
            {
                return changeRotateCommandUp ?? (changeRotateCommandUp = new CustomCommand(action =>
                {
                    _machinesList.TWSQuadroMachineSelected.RotateChangeUp();
                }, exec => _machinesList.TWSQuadroMachineSelected != null));
            }
        }

        CustomCommand changeRotateCommandDown;
        public CustomCommand ChangeRotateCommandDown
        {
            get
            {
                return changeRotateCommandDown ?? (changeRotateCommandDown = new CustomCommand(action =>
                {
                    _machinesList.TWSQuadroMachineSelected.RotateChangeDown();
                }, exec => _machinesList.TWSQuadroMachineSelected != null));
            }
        }

        CustomCommand assemblyFileToggleCommand;
        public CustomCommand AssemblyFileToggleCommand
        {
            get
            {
                return assemblyFileToggleCommand ?? (assemblyFileToggleCommand = new CustomCommand(action =>
                {
                    var selectedMachine = _machinesList.TWSQuadroMachineSelected;
                    selectedMachine.IsAssemblyFile = !selectedMachine.IsAssemblyFile;
                }, exec => _machinesList.TWSQuadroMachineSelected != null));
            }
        }

    }
}
