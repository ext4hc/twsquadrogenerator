﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using AltiumDesignerParser.Services;

namespace AltiumDesignerParser.ViewModel
{
    class MenuItemOutputViewModel
    {
        MenuItemFileViewModel _fileVM;

        public MenuItemOutputViewModel(MenuItemFileViewModel fileVM)
        {
            _fileVM = fileVM;
        }


        CustomCommand createAllLayser;
        public CustomCommand CreateAllLayer => createAllLayser ?? (createAllLayser = new CustomCommand(param =>
        {

            List<string> OtputFileStrings = new List<string>();

            foreach (var element in _fileVM.TWSQuadroMachinesList.Where(p => p.IsAssemblyFile == true))
            {
                OtputFileStrings.Add(MachineParam.CreateOutpuString(element));
            }

            FileService.CreateFileASQ(OtputFileStrings);
            
        }, param => _fileVM.TWSQuadroMachinesList.Count != 0));

        CustomCommand createTopLayer;
        public CustomCommand CreateTopLayer => createTopLayer ?? (createTopLayer = new CustomCommand(param =>
        {
            List<string> OutputFilesString = new List<string>();

            foreach (var element in _fileVM.TWSQuadroMachinesList.Where(p => p.Layer == "TopLayer").Where(p => p.IsAssemblyFile == true))
            {
                OutputFilesString.Add(MachineParam.CreateOutpuString(element));
            }
            
            FileService.CreateFileASQ(OutputFilesString);

        }, param => _fileVM.TWSQuadroMachinesList.Count != 0));

        CustomCommand createBottomLayer;
        public CustomCommand CreateBottomLayer => createBottomLayer ?? (createBottomLayer = new CustomCommand(param =>
        {
            List<string> OutputFilesString = new List<string>();

            foreach (var element in _fileVM.TWSQuadroMachinesList.Where(p => p.Layer == "BottomLayer").Where(p => p.IsAssemblyFile == true))
            {
                OutputFilesString.Add(MachineParam.CreateOutpuString(element));
            }

            FileService.CreateFileASQ(OutputFilesString);
            
        }, param => _fileVM.TWSQuadroMachinesList.Count != 0));
    }
}
