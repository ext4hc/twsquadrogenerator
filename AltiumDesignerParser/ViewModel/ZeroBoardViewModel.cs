﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AltiumDesignerParser.Services;

namespace AltiumDesignerParser.ViewModel
{
    class ZeroBoardViewModel : Notify
    {
        MenuItemFileViewModel _fileVM;

        public ZeroBoardViewModel(MenuItemFileViewModel fileVM)
        {
            _fileVM = fileVM;
        }

        Decimal contentX;
        public Decimal ContentX
        {
            get => contentX;
            set
            {
                contentX = value;
                NotifyPropertyChanged();
            }
        }

        Decimal contentY;
        public Decimal ContentY
        {
            get => contentY;
            set
            {
                contentY = value;
                NotifyPropertyChanged();
            }
        }

        CustomCommand saveChange;
        public CustomCommand SaveChange => saveChange ?? (saveChange = new CustomCommand(param =>
        {
            Decimal X;
            Decimal Y;
            foreach(var el in _fileVM.TWSQuadroMachinesList)
            {
                X = new Decimal(el.CoordinateX) + ContentX;
                Y = new decimal(el.CoordinateY) + ContentY;

                el.CoordinateX = Decimal.ToSingle(X);
                el.CoordinateY = Decimal.ToSingle(Y);
            }
        }, param => true));
    }
}
