﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AltiumDesignerParser.Model;
using AltiumDesignerParser.Services;

namespace AltiumDesignerParser.ViewModel
{
    class MenuItemPackageViewModel : Notify
    {
        MenuItemPackageModel _packageModel;
        public CustomCommand Command { get; set; }

        public string Content
        {
            get => _packageModel.Name;
            set
            {
                _packageModel.Name = value;
                NotifyPropertyChanged();
            }
        }

        public MenuItemPackageViewModel(MenuItemPackageModel packageModel, CustomCommand command)
        {
            _packageModel = packageModel;
            Command = command;
        }
    }
}
