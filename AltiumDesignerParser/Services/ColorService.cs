﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltiumDesignerParser.Services
{
    class ColorService
    {
        public static string FalseComponentColor { get; set; } = "#FFFCC0B9";
        public static string TrueComponentColor { get; set; } = "#FF97DC65";
    }
}
