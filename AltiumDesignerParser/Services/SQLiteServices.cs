﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;
using System.Data;
using AltiumDesignerParser.Model;
using System.Data.Common;

namespace AltiumDesignerParser.Services
{
    class SQLiteServices
    {
        private static string _filename;
        private static string _nametable;


        public SQLiteServices()
        {
        }


        public static void CreateDataBase(string filename)
        {
            _filename = filename;
            if(!File.Exists(filename))
            {
                SQLiteConnection.CreateFile(filename);
            }
        }

        public static void CreateTable(string nametable)
        {
            _nametable = nametable;
            using (SQLiteConnection Connection = new SQLiteConnection(string.Format("Data Source={0};", _filename)))
            {
                string customCommand = $"CREATE TABLE IF NOT EXISTS {_nametable} (AltiumComponent TEXT PRIMARY KEY, MachineComponent TEXT);";
                SQLiteCommand Command = new SQLiteCommand(customCommand, Connection);
                Connection.Open();
                Command.ExecuteNonQuery();
                Connection.Close();
            }
        }

        public static async void InsertEntry(Component component)
        {
            await Task.Run(() =>
            {
                string searchElement = SearchEntry(component);

                if(searchElement != string.Empty)
                {
                    if(component.MachineComponent != searchElement)
                    {
                        using (SQLiteConnection Connection = new SQLiteConnection(string.Format("Data Source={0};", _filename)))
                        {
                            string cmd = $"UPDATE {_nametable} SET MachineComponent='{component.MachineComponent}' WHERE AltiumComponent='{component.AltiumComponent}'";

                            SQLiteCommand Command = new SQLiteCommand(cmd, Connection);
                            Connection.Open();
                            Command.ExecuteNonQuery();
                            Connection.Close();
                        }
                    }
                }
                else
                {
                    using (SQLiteConnection Connecton = new SQLiteConnection(string.Format("Data Source={0};", _filename)))
                    {
                        string cmd = $"INSERT INTO Components ('AltiumComponent', 'MachineComponent') VALUES ('{component.AltiumComponent}', '{component.MachineComponent}');";
                        SQLiteCommand Command = new SQLiteCommand(cmd, Connecton);

                        Connecton.Open();
                        Command.ExecuteNonQuery();
                        Connecton.Close();
                    }
                }
            });            
        }

        public static string SearchEntry(Component component)
        {
            using (SQLiteConnection Connection = new SQLiteConnection(string.Format("Data Source={0};", _filename)))
            {
                string customCommand = $"SELECT MachineComponent FROM {_nametable} WHERE AltiumComponent='{component.AltiumComponent}'";
                SQLiteCommand Command = new SQLiteCommand(customCommand, Connection);
                Connection.Open();
                SQLiteDataReader Reader = Command.ExecuteReader();

                string result = string.Empty;
                foreach (DbDataRecord record in Reader)
                {
                    result = record["MachineComponent"].ToString();
                }

                Connection.Close();
                return result;
            }
        }

        public static List<Component> GetAllDataFromDataBase()
        {
            List<Component> Result = new List<Component>();
            using (SQLiteConnection Connection = new SQLiteConnection(string.Format("Data Source={0};", _filename)))
            {
                string customCommand = $"SELECT * FROM {_nametable}";
                SQLiteCommand Command = new SQLiteCommand(customCommand, Connection);
                Connection.Open();
                SQLiteDataReader Reader = Command.ExecuteReader();

                foreach(DbDataRecord record in Reader)
                {
                    Result.Add(new Component()
                    {
                        AltiumComponent = record["AltiumComponent"].ToString(),
                        MachineComponent = record["MachineComponent"].ToString()
                    });
                }

                Connection.Close();
            }

            return Result;
        }

        public static void AMS()
        {
            //List<Component> Result = new List<Component>();
            Decimal one = new Decimal(23.2);
            Decimal two = one - new decimal(23.11);
        }
    }
}
