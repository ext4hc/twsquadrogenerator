﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows;
using Microsoft.Win32;

namespace AltiumDesignerParser.Services
{
    class FileService
    {
        FileService()
        {

        }

        public static void CreateFileASQ(IEnumerable<string> source)
        {

            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                AddExtension = true,
                InitialDirectory = "D:\\",
                DefaultExt = "asq"
            };

            if (saveFileDialog.ShowDialog() == true)
            {
                File.WriteAllLines($"{saveFileDialog.FileName}", source);
            }
        }
    }
}
