﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltiumDesignerParser.Services
{
    class MachineComponents
    {
        private static readonly MachineComponents instance = new MachineComponents();

        public List<string> Components { get; set; }

        private MachineComponents()
        {
            Components = new List<string>();

            string[] lines = File.ReadAllLines("AvailableComponents.txt");

            foreach (string line in lines)
            {
                Components.Add(line.Trim(' '));
            }
        }

        public static MachineComponents GetInstance()
        {
            return instance;
        }
    }
}
