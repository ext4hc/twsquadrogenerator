﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace AltiumDesignerParser
{
    class CustomCommand : ICommand
    {
        private Action<object> action;
        private Func<object, bool> execute;
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public CustomCommand(Action<object> action, Func<object, bool> execute = null)
        {
            this.action = action;
            this.execute = execute;
        }

        public bool CanExecute(object parameter)
        {
            return execute(parameter);
        }

        public void Execute(object parameter)
        {
            action(parameter);
        }
    }
}
