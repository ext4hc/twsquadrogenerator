﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltiumDesignerParser.Services
{
    class FieldOutputFile
    {
        public static string FormateField(string val, bool isStringFormat, bool isLastField = false)
        {
            val = isStringFormat ? $"#{val}#" : val;
            return isLastField ? val : $"{val},";
        }

        public static string FormateOuputString(params string[] fields)
        {
            string result = "";
            foreach (string val in fields)
            {
                result += val;
            }
            return result;
        }

    }

    class MachineParam
    {
        static string Designator { get; set; } = string.Empty;// codeComp
        static string CoordinateX { get; set; } = string.Empty;
        static string CoordinateY { get; set; } = string.Empty;
        static string Rotate { get; set; } = string.Empty;
        static string PinZ { get; set; } = "PXY";// PinZ
        static string Package { get; set; } = string.Empty;
        static string Comments { get; set; } // TipoComponent
        static string NC2 { get; set; } = "1"; // N.C val: 1
        static string NC3 { get; set; } = "T"; // N.C val: T
        static string NCx { get; set; } = "1"; // N.C val 1(false)
        static string Feeder { get; set; } // Punta(feeder) val: #1#
        static string NC4 { get; set; } = "F"; // N.C val: F
        static string NC5 { get; set; } = "TAPE";// N.C val: #TAPE#
        static string Orient { get; set; } = "X"; // N.C(Orient) val: #Y#
        static string Nozzle { get; set; } = "A";// Ugello val: #A#
        static string NC6 { get; set; } = "G";// val ##
        static string NC7 { get; set; } = "";// val ##
        static string NC8 { get; set; } = "F";// val F

        public MachineParam()
        {

        }

        public MachineParam(TWSQuadroMachineViewModel Aparam)
        {
            //Designator = Aparam.Designator;
            //CoordinateX = Aparam.CoordinateX.ToString().Replace(",", ".");
            //CoordinateY = Aparam.CoordinateY.ToString().Replace(",", ".");
            //Rotate = Aparam.Rotate.ToString().Replace(",", ".");
            //Comments = Aparam.Comments;
            //Feeder = Aparam.Feeder;
            //Package = Aparam.Package;
            //Nozzle = Aparam.Nozzle;
        }


        static public string CreateOutpuString(TWSQuadroMachineViewModel Aparam)
        {
            Designator = Aparam.Designator;
            CoordinateX = Aparam.CoordinateX.ToString().Replace(",", ".");
            CoordinateY = Aparam.CoordinateY.ToString().Replace(",", ".");
            Rotate = Aparam.Rotate.ToString().Replace(",", ".");
            Comments = Aparam.TipoComponent;
            Feeder = Aparam.Feeder;
            Package = Aparam.Package;
            //Nozzle = Aparam.Nozzle;

            StringBuilder builder = new StringBuilder();
            return FieldOutputFile.FormateOuputString(FieldOutputFile.FormateField(Designator, true),
                                                      FieldOutputFile.FormateField(CoordinateX, false),
                                                      FieldOutputFile.FormateField(CoordinateY, false),
                                                      FieldOutputFile.FormateField(Rotate, false),
                                                      FieldOutputFile.FormateField(PinZ, true),
                                                      FieldOutputFile.FormateField(Package, true),
                                                      FieldOutputFile.FormateField(Comments, true),
                                                      FieldOutputFile.FormateField(NC2, false),
                                                      FieldOutputFile.FormateField(NC3, false),
                                                      FieldOutputFile.FormateField(NCx, true),
                                                      FieldOutputFile.FormateField(Feeder, false),
                                                      FieldOutputFile.FormateField(NC4, false),
                                                      FieldOutputFile.FormateField(NC5, true),
                                                      FieldOutputFile.FormateField(Orient, true),
                                                      FieldOutputFile.FormateField(Nozzle, true),
                                                      FieldOutputFile.FormateField(NC6, true),
                                                      FieldOutputFile.FormateField(NC7, true),
                                                      FieldOutputFile.FormateField(NC8, false, true));
        }
    }
}
