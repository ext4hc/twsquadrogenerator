﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Input;
using AltiumDesignerParser.Services;

namespace AltiumDesignerParser.Services
{
    abstract class Parser
    {
        protected string filename;
        protected string[] ReadData;

        public Parser(string filename)
        {
            this.filename = filename;
        }

        public virtual bool ReadFile()
        {
            if (File.Exists(filename))
            {
                ReadData = File.ReadAllLines(filename);
                return true;
            }
            else
            {
                return false;
            }
        }
    }


    class AltiumParam
    {
        const int maxDegree = 360;
        const int stepDegree = 90;


        const int designatorOffset  = 0;
        const int commentOffset     = 1;
        const int layerOffset       = 2;
        const int footprintOffset   = 3;
        const int centerXOffset     = 4;
        const int centerYOffset     = 5;
        const int rotationOffest    = 6;
        const int descriptionOffset = 7;

        private AltiumParam(string[] AParams)
        {
            Designator = AParams[designatorOffset].Replace("\"", "");
            Comments = AParams[commentOffset].Replace("\"", "");
            Layer = AParams[layerOffset].Replace("\"", "");
            FootPrint = AParams[footprintOffset].Replace("\"", "");

            AParams[centerXOffset] = AParams[centerXOffset].Replace('.', ',');
            AParams[centerYOffset] = AParams[centerYOffset].Replace('.', ',');

            CenterX = Single.Parse(AParams[centerXOffset].Replace("\"", ""));
            CenterY = Single.Parse(AParams[centerYOffset].Replace("\"", ""));
            Rotation = Single.Parse(AParams[rotationOffest].Replace("\"", ""));

            if (Layer == "BottomLayer")
            {
                CenterX = -CenterX;
            }
        }

        public static AltiumParam Create(string[] Parameters)
        {
            return new AltiumParam(Parameters);
        }


        //public string Date { get; set; }
        //public string Time { get; set; }
        //public string UnitsUser { get; set; }

        public string Designator { get; set; }
        public string Comments { get; set; }
        public string Layer { get; set; }
        public string FootPrint { get; set; }

        public float CenterX { get; set; }
        public float CenterY { get; set; }

        public float Rotation { get; set; }

    }

    class AltiumParse : Parser
    {
        const int offsetDate = 6;
        const int offsetTime = 7;
        const int offsetUnits = 10;
        const int offsetHeaderParam = 12;

        const int offsetElements = 13;

        public AltiumParse(string filename) : base(filename)
        {
            ReadFile();
        }

        public List<AltiumParam> Parse()
        {
            List<AltiumParam> Params = new List<AltiumParam>();
            //string pattern = "\"-{0,1}\\d{1,},\\d{1,}\"";

            for (int i = offsetElements; i < ReadData.Length; i++)
            {
                //string newValue = "";
                //var collection = Regex.Matches(ReadData[i], pattern);

                //foreach(Match res in collection)
                //{
                //    newValue = res.Value.Replace(",", ".");
                //    ReadData[i] = ReadData[i].Replace(res.Value, newValue);
                //}

                //Params.Add(AltiumParam.Create(ReadData[i].Split(',')));
                string[] separator = new string[] { "\",\"" };

                Params.Add(AltiumParam.Create(ReadData[i].Split(separator, StringSplitOptions.None)));

                //Params.Add(AltiumParam.Create(ReadData[i].Split("\"\"", StringSplitOptions.None)));
            }

            return Params;
        }
        
    }

    class Feeder
    {
        const int FeederOffset = 0;
        const int ComponentTypeOffset = 1;
        const int PackageOffset = 2;
        const int NoteOffset = 3;
        const int AOffset = 4;
        const int TOffset = 5;
        const int QuantityOffset = 6;


        public string FeederVal { get; set; }
        public string Component { get; set; }
        public string Package { get; set; }
        public string Note { get; set; }
        public string A { get; set; }
        public string T { get; set; }
        public string Quantity { get; set; }

        private Feeder(string inputString)
        {
            string[] result = inputString.Split(',');
            FeederVal = result[FeederOffset];
            Component = result[ComponentTypeOffset];
            Package = result[PackageOffset];
            Note = result[NoteOffset];
            A = result[AOffset];
            T = result[TOffset];
            Quantity = result[QuantityOffset];
        }

        public static Feeder Create(string inputString)
        {
            return new Feeder(inputString);
        }
    }

    class FeederParse : Parser
    {

        public FeederParse(string filename) : base(filename)
        {
            ReadFile();
        }

        public List<Feeder> Parse()
        {
            List<Feeder> FeederParams = new List<Feeder>();

            foreach(var inputStr in ReadData)
            {
                FeederParams.Add(Feeder.Create(inputStr));
            }

            return FeederParams;
        }
    }
}
