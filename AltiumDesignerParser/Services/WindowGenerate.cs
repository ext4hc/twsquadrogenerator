﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AltiumDesignerParser.Services
{
    class WindowGenerate
    {
        public static void CustomWinCreate<Win>(object vm) where Win : Window
        {
            Window window = (Window)Activator.CreateInstance(typeof(Win));
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            window.DataContext = vm;
            window.Show();
        }
    }
}
